package com.hcloud.auth.api.permission;

import com.hcloud.auth.api.util.AuthUtil;
import com.hcloud.system.core.bean.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * @Auther hepangui
 * @Date 2018/11/14
 */
@Slf4j
@Service("ACS")
public class AuthorityCheckService {

    public boolean has(String authority) {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        var user = AuthUtil.getUser(authentication);
        //用超管帐号方便测试，拥有所有权限
        var account = user.getAccount();
        if("hepg".equals(account)){
            return true;
        }
        if(authority.contains("edit") || authority.contains("del")){
            return false;
        }
        return   AuthUtil.getAuthority().stream().anyMatch(a -> authority.equals(a));
//
//        var a = authorities.stream().takeWhile(grantedAuthority
//                -> !grantedAuthority.getAuthority().equals(authority)).count();
//        return a != authorities.size();
//
//        for (GrantedAuthority grantedAuthority : authorities) {
//            String authority1 = grantedAuthority.getAuthority();
//            if(authority1!=null && authority1.equals(authority)){
//                return true;
//            }
//        }
//        return false;
    }

    public static void test(String[] args) {
        //java9新特性
        Stream.of("a", "b", "c", "", "e", "f").dropWhile(s -> !s.isEmpty())
                .forEach(System.out::print);

        Stream.of("a", "b", "c", "", "e", "f").takeWhile(s -> !s.isEmpty())
                .forEach(System.out::print);
    }
}
