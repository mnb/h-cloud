//package com.hcloud.auth.api;
//
//import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
//import org.springframework.cloud.openfeign.EnableFeignClients;
//import org.springframework.context.annotation.Configuration;
//
///**
// * 当某一个web项目引入此依赖时，自动配置对应的内容
// * 初始化log的事件监听与切面配置
// *
// * @Auther hepangui
// * @Date 2018/11/20
// */
//@Configuration
//@ConditionalOnWebApplication
//@EnableFeignClients({"com.hcloud.audit.api.feign"})
//public class AuthAutoConfiguration {
//
//}
