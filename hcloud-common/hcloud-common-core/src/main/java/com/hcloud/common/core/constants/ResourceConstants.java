package com.hcloud.common.core.constants;

/**
 * 资源服务器常量
 * @Auther hepangui
 * @Date 2018/11/12
 */
public interface ResourceConstants {

    String AUTH = "auth";
    String SYSTEM = "system";
    String AUDIT = "audit";
}
