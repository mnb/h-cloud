/*
Navicat MySQL Data Transfer
Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001
Date: 2018-11-30 13:05:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for h_audit_login_log
-- ----------------------------
DROP TABLE IF EXISTS `h_audit_login_log`;
CREATE TABLE `h_audit_login_log` (
                                   `id` varchar(255) NOT NULL,
                                   `create_time` datetime DEFAULT NULL,
                                   `update_time` datetime DEFAULT NULL,
                                   `parent_id` varchar(255) DEFAULT NULL,
                                   `browser` varchar(255) DEFAULT NULL,
                                   `device` varchar(255) DEFAULT NULL,
                                   `ip-addr` varchar(255) DEFAULT NULL,
                                   `os_name` varchar(255) DEFAULT NULL,
                                   `success` int(11) DEFAULT NULL,
                                   `username` varchar(255) DEFAULT NULL,
                                   `ip_addr` varchar(255) DEFAULT NULL,
                                   `account` varchar(255) DEFAULT NULL,
                                   `msg` varchar(255) DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   KEY `idx_name` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_audit_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for h_audit_opreate_log
-- ----------------------------
DROP TABLE IF EXISTS `h_audit_opreate_log`;
CREATE TABLE `h_audit_opreate_log` (
                                     `id` varchar(255) NOT NULL,
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     `parent_id` varchar(255) DEFAULT NULL,
                                     `ip` varchar(255) DEFAULT NULL,
                                     `operate` varchar(255) DEFAULT NULL,
                                     `params` varchar(255) DEFAULT NULL,
                                     `time` bigint(20) DEFAULT NULL,
                                     `uri` varchar(255) DEFAULT NULL,
                                     `username` varchar(255) DEFAULT NULL,
                                     `type` varchar(255) DEFAULT NULL,
                                     PRIMARY KEY (`id`),
                                     KEY `idx_name` (`username`),
                                     KEY `idx_createtime` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_audit_opreate_log
-- ----------------------------

-- ----------------------------
-- Table structure for h_system_authority
-- ----------------------------
DROP TABLE IF EXISTS `h_system_authority`;
CREATE TABLE `h_system_authority` (
                                    `id` varchar(255) NOT NULL,
                                    `create_time` datetime DEFAULT NULL,
                                    `update_time` datetime DEFAULT NULL,
                                    `authority` varchar(255) DEFAULT NULL,
                                    `is_menu` int(11) DEFAULT NULL,
                                    `menu_icon` varchar(255) DEFAULT NULL,
                                    `menu_url` varchar(255) DEFAULT NULL,
                                    `name` varchar(255) DEFAULT NULL,
                                    `order_num` int(11) DEFAULT NULL,
                                    `parent_id` varchar(255) DEFAULT NULL,
                                    `menu_href` varchar(255) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `idx_name` (`name`),
                                    KEY `idx_parnetId` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_system_authority
-- ----------------------------
INSERT INTO `h_system_authority` VALUES ('2c94008166d82e070166da36e3e10005', '2018-11-14 18:35:50', '2018-11-14 18:35:50', '', '1', 'layui-icon layui-icon-set', '', '系统管理', '1', '-1', null);
INSERT INTO `h_system_authority` VALUES ('2c94008166d82e070166da383c5d0006', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-auth-see', '1', '', '/view/system/auth/auth.html', '权限管理', '1', '2c94008166d82e070166da36e3e10005', '#!sys-auth');
INSERT INTO `h_system_authority` VALUES ('2c94008166d82e070166da38c0c30007', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-role-see', '1', '', '/view/system/role/role.html', '角色管理', '5', '2c94008166d82e070166da36e3e10005', '#!sys-role');
INSERT INTO `h_system_authority` VALUES ('2c94008166e390420166e6a30d0c0000', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-user-see', '1', '', '/view/system/user/user.html', '用户管理', '15', '2c94008166d82e070166da36e3e10005', '#!user');
INSERT INTO `h_system_authority` VALUES ('2c9400816706977701670734b0630000', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-auth-add', '0', '', '', '添加权限', '1', '2c94008166d82e070166da383c5d0006', '');
INSERT INTO `h_system_authority` VALUES ('2c9400816711c59d016711ca9a8e0000', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-user-edit', '0', '', '', '修改用户', '2', '2c94008166e390420166e6a30d0c0000', '');
INSERT INTO `h_system_authority` VALUES ('2c940081671227370167151a82740020', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-role-add', '0', '', '', '添加角色', '2', '2c94008166d82e070166da38c0c30007', '');
INSERT INTO `h_system_authority` VALUES ('2c940081671227370167153999aa0029', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-auth-edit', '0', '', '', '编辑权限', '3', '2c94008166d82e070166da383c5d0006', '');
INSERT INTO `h_system_authority` VALUES ('2c94008167122737016715417a320035', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-auth-del', '0', '', '', '删除权限', '5', '2c94008166d82e070166da383c5d0006', '');
INSERT INTO `h_system_authority` VALUES ('2c94008167122737016715423d190036', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-role-edit', '0', '', '', '编辑角色', '3', '2c94008166d82e070166da38c0c30007', '');
INSERT INTO `h_system_authority` VALUES ('2c9400816712273701671542a03a0037', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-role-del', '0', '', '', '删除角色', '5', '2c94008166d82e070166da38c0c30007', '');
INSERT INTO `h_system_authority` VALUES ('2c940081671227370167154353f20038', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-user-add', '0', '', '', '添加用户', '1', '2c94008166e390420166e6a30d0c0000', '');
INSERT INTO `h_system_authority` VALUES ('2c9400816712273701671543fb1a0039', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-user-del', '0', '', '', '删除用户', '5', '2c94008166e390420166e6a30d0c0000', '');
INSERT INTO `h_system_authority` VALUES ('4028824867598d2b016759a5b0f20016', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'sys-monitor-see', '1', '', 'http://127.0.0.1:1075/#/wallboard', '系统监控', '12', '40288248675ef74401675f09ed16001a', '#!monitor');
INSERT INTO `h_system_authority` VALUES ('40288248675d14cd01675d592c340000', '2018-11-14 18:35:50', '2018-11-14 18:35:50', '', '1', 'layui-icon layui-icon-survey', '', '日志审计', '15', '-1', '');
INSERT INTO `h_system_authority` VALUES ('40288248675d14cd01675d5a589e0001', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'audit-login-see', '1', '', '/view/audit/login/loginlog.html', '登录日志', '1', '40288248675d14cd01675d592c340000', '#!audit-login');
INSERT INTO `h_system_authority` VALUES ('40288248675d14cd01675d5b80b90002', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'audit-operate-see', '1', '', '/view/audit/operate/operatelog.html', '操作日志', '5', '40288248675d14cd01675d592c340000', '#!audit-operate');
INSERT INTO `h_system_authority` VALUES ('40288248675df5d701675e9387800000', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'audit-login-del', '0', '', '', '删除登录日志', '1', '40288248675d14cd01675d5a589e0001', '');
INSERT INTO `h_system_authority` VALUES ('40288248675ef74401675f09ed16001a', '2018-11-14 18:35:50', '2018-11-14 18:35:50', '', '1', 'layui-icon layui-icon-console', '', '系统监控', '15', '-1', '');
INSERT INTO `h_system_authority` VALUES ('402882486762c911016762ed33390002', '2018-11-14 18:35:50', '2018-11-14 18:35:50', 'audit-operate-del', '0', '', '', '删除操作日志', '3', '40288248675d14cd01675d5b80b90002', '');

-- ----------------------------
-- Table structure for h_system_role
-- ----------------------------
DROP TABLE IF EXISTS `h_system_role`;
CREATE TABLE `h_system_role` (
                               `id` varchar(255) NOT NULL,
                               `create_time` datetime DEFAULT NULL,
                               `update_time` datetime DEFAULT NULL,
                               `comment` varchar(255) DEFAULT NULL,
                               `name` varchar(255) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_system_role
-- ----------------------------
INSERT INTO `h_system_role` VALUES ('2c94008166e36e2c0166e36ff21a0004', '2018-11-14 18:35:50', '2018-11-15 10:56:58', '超级管理员', '系统管理员');
INSERT INTO `h_system_role` VALUES ('2c940081671670220167167aa5750014', '2018-11-14 18:35:50', '2018-11-30 12:45:20', '测试用', '测试角色');

-- ----------------------------
-- Table structure for h_system_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `h_system_role_auth`;
CREATE TABLE `h_system_role_auth` (
                                    `id` varchar(255) NOT NULL,
                                    `create_time` datetime DEFAULT NULL,
                                    `update_time` datetime DEFAULT NULL,
                                    `auth_id` varchar(255) DEFAULT NULL,
                                    `role_id` varchar(255) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `idx_roleId` (`role_id`),
                                    KEY `idx_authId` (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_system_role_auth
-- ----------------------------
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e3512b0166e358454e0008', null, null, '-1', '2c94008166e3512b0166e3524c580000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e3512b0166e358454f0009', null, null, '2c94008166d82e070166da36e3e10005', '2c94008166e3512b0166e3524c580000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e3512b0166e358454f000a', null, null, '2c94008166d82e070166da383c5d0006', '2c94008166e3512b0166e3524c580000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e3512b0166e358454f000b', null, null, '2c94008166d82e070166da38c0c30007', '2c94008166e3512b0166e3524c580000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e3512b0166e3584550000c', null, null, '2c94008166d82e070166da3922200008', '2c94008166e3512b0166e3524c580000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e36e2c0166e37b302b0009', null, null, '-1', '2c94008166e33bb70166e340aeaf0000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e36e2c0166e37b302b000a', null, null, '2c94008166d82e070166da36e3e10005', '2c94008166e33bb70166e340aeaf0000');
INSERT INTO `h_system_role_auth` VALUES ('2c94008166e36e2c0166e37b302b000b', null, null, '2c94008166d82e070166da38c0c30007', '2c94008166e33bb70166e340aeaf0000');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42030018', null, null, '-1', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42040019', null, null, '2c94008166d82e070166da36e3e10005', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4204001a', null, null, '2c94008166d82e070166da383c5d0006', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4204001b', null, null, '2c9400816706977701670734b0630000', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4204001c', null, null, '2c940081671227370167153999aa0029', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4204001d', null, null, '2c94008167122737016715417a320035', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4204001e', null, null, '2c94008166d82e070166da38c0c30007', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4204001f', null, null, '2c940081671227370167151a82740020', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42040020', null, null, '2c94008167122737016715423d190036', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42040021', null, null, '2c9400816712273701671542a03a0037', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42040022', null, null, '2c94008166e390420166e6a30d0c0000', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050023', null, null, '2c9400816711c59d016711ca9a8e0000', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050024', null, null, '2c940081671227370167154353f20038', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050025', null, null, '2c9400816712273701671543fb1a0039', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050026', null, null, '40288248675d14cd01675d592c340000', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050027', null, null, '40288248675d14cd01675d5a589e0001', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050028', null, null, '40288248675df5d701675e9387800000', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee42050029', null, null, '40288248675d14cd01675d5b80b90002', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4205002a', null, null, '402882486762c911016762ed33390002', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4205002b', null, null, '40288248675ef74401675f09ed16001a', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee4205002c', null, null, '4028824867598d2b016759a5b0f20016', '2c940081671670220167167aa5750014');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ab002d', null, null, '-1', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ab002e', null, null, '2c94008166d82e070166da36e3e10005', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ab002f', null, null, '2c94008166d82e070166da383c5d0006', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ab0030', null, null, '2c9400816706977701670734b0630000', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ab0031', null, null, '2c940081671227370167153999aa0029', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ac0032', null, null, '2c94008167122737016715417a320035', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ac0033', null, null, '2c94008166d82e070166da38c0c30007', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ac0034', null, null, '2c940081671227370167151a82740020', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ad0035', null, null, '2c94008167122737016715423d190036', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ad0036', null, null, '2c9400816712273701671542a03a0037', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ad0037', null, null, '2c94008166e390420166e6a30d0c0000', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ad0038', null, null, '2c9400816711c59d016711ca9a8e0000', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ad0039', null, null, '2c940081671227370167154353f20038', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ad003a', null, null, '2c9400816712273701671543fb1a0039', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae003b', null, null, '40288248675d14cd01675d592c340000', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae003c', null, null, '40288248675d14cd01675d5a589e0001', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae003d', null, null, '40288248675df5d701675e9387800000', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae003e', null, null, '40288248675d14cd01675d5b80b90002', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae003f', null, null, '402882486762c911016762ed33390002', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae0040', null, null, '40288248675ef74401675f09ed16001a', '2c94008166e36e2c0166e36ff21a0004');
INSERT INTO `h_system_role_auth` VALUES ('402882486762c911016762ee59ae0041', null, null, '4028824867598d2b016759a5b0f20016', '2c94008166e36e2c0166e36ff21a0004');

-- ----------------------------
-- Table structure for h_system_user
-- ----------------------------
DROP TABLE IF EXISTS `h_system_user`;
CREATE TABLE `h_system_user` (
                               `id` varchar(255) NOT NULL,
                               `create_time` datetime DEFAULT NULL,
                               `update_time` datetime DEFAULT NULL,
                               `account` varchar(255) DEFAULT NULL,
                               `mobile` varchar(255) DEFAULT NULL,
                               `name` varchar(255) DEFAULT NULL,
                               `password` varchar(255) DEFAULT NULL,
                               `qq` varchar(255) DEFAULT NULL,
                               `role` varchar(255) DEFAULT NULL,
                               `sex` int(11) DEFAULT NULL,
                               `state` int(11) DEFAULT NULL,
                               `wechat` varchar(255) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `idx_name` (`name`),
                               KEY `idx_account` (`account`),
                               KEY `idx_mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_system_user
-- ----------------------------
INSERT INTO `h_system_user` VALUES ('2c94008166f232b60166f23340cf0000', '2018-11-14 18:35:50', '2018-11-30 12:45:05', 'hepg', '18888888888', '管理员', '$2a$10$9xma5IWV7g89vPPcGNj88OH53ls5j5stbawve84WzfSpfk5ynVUtq', '135426708', '2c94008166e36e2c0166e36ff21a0004', '1', '1', 'guitongxue2012');
INSERT INTO `h_system_user` VALUES ('2c9f2ea7672b5dac01672b6424ae0000', '2018-11-14 18:35:50', '2018-11-28 16:35:03', 'test', '13623698696', '测试帐号', '$2a$10$saax8/8/HmLDktDEsNgw3OEl577Sh.78IxGkGDy8HwrUVjfLDQ.GS', '123456789', '2c940081671670220167167aa5750014', '1', '1', '123456');

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
                                      `client_id` varchar(128) NOT NULL,
                                      `resource_ids` varchar(256) DEFAULT NULL,
                                      `client_secret` varchar(256) DEFAULT NULL,
                                      `scope` varchar(256) DEFAULT NULL,
                                      `authorized_grant_types` varchar(256) DEFAULT NULL,
                                      `web_server_redirect_uri` varchar(256) DEFAULT NULL,
                                      `authorities` varchar(256) DEFAULT NULL,
                                      `access_token_validity` int(11) DEFAULT NULL,
                                      `refresh_token_validity` int(11) DEFAULT NULL,
                                      `additional_information` varchar(4096) DEFAULT NULL,
                                      `autoapprove` varchar(256) DEFAULT NULL,
                                      PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('font', 'auth,system,audit', '$2a$10$9xma5IWV7g89vPPcGNj88OH53ls5j5stbawve84WzfSpfk5ynVUtq', 'server', 'password', null, null, null, null, null, 'true');
