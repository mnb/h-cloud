package com.hcloud.system.core.bean.user;

import lombok.Data;
import com.hcloud.common.core.base.BaseBean;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 用户
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@Accessors(chain = true)
public class User extends BaseBean {

    @NotBlank(message = "帐号不能为空")
    private String account;
    @NotBlank(message = "用户名不能为空")
    private String name;
    private Integer sex;
    private String password;
    private String wechat;
    private String qq;
    private String mobile;
    @NotBlank(message = "角色不能为空")
    private String role;
    @NotNull(message = "状态不能为空")
    private Integer state;

}
