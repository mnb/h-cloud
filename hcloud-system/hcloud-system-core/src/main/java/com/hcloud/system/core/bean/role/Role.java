package com.hcloud.system.core.bean.role;

import lombok.Data;
import com.hcloud.common.core.base.BaseBean;

/**
 * 角色
 * @Auther hepangui
 * @Date 2018/11/5
 *
 */
@Data
public class Role extends BaseBean {
    private String name;
    private String comment;
}
