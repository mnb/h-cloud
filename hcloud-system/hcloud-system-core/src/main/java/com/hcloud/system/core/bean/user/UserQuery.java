package com.hcloud.system.core.bean.user;

import lombok.Data;

import java.util.Date;

/**
 * 用户模糊查询条件
 * @Auther hepangui
 * @Date 2018/11/6
 */
@Data
public class UserQuery extends User{
    private String nameLike;
    private Date createTimeGt;
    private Date createTimeLt;
}
