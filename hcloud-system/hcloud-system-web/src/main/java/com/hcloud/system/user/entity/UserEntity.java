package com.hcloud.system.user.entity;

import com.hcloud.common.crud.entity.BaseEntity;
import com.hcloud.common.crud.entity.BaseTreeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@Entity
@Accessors(chain = true)
@Table(name = "h_system_user",
        indexes = {@Index(name = "idx_name", columnList = "name")
                , @Index(name = "idx_account", columnList = "account"),
                @Index(name="idx_mobile",columnList = "mobile")}

)
public class UserEntity extends BaseEntity {
    private String account;
    private String name;
    private Integer sex;
    private String wechat;
    private String qq;
    private String mobile;
    @Column(name = "role",length = 500)
    private String role;
    private Integer state;
    private String password;

}
