package com.hcloud.system.user.service;

import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.system.core.bean.user.User;
import com.hcloud.system.user.entity.UserEntity;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface UserService
        extends BaseDataService<UserEntity, User> {
    User findByAccount(String account);

    User findByMobile(String mobile);

    User findByWeixin(String weixin);

    User findByQq(String qq);

    void modifyPass(User user, String oldPsw, String newPsw);

    void resetPass(String userId);
}
