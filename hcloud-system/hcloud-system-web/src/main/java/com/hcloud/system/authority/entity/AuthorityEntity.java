package com.hcloud.system.authority.entity;

import com.hcloud.common.crud.entity.BaseTreeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "h_system_authority",
        indexes = {@Index(name = "idx_name", columnList = "name")
                , @Index(name = "idx_parnetId", columnList = "parentId")}

)
public class AuthorityEntity extends BaseTreeEntity {
    private String name;
    private Integer orderNum;
    private String menuHref;
    private String menuUrl;
    private String menuIcon;
    private String authority;
    private Integer isMenu;

}
