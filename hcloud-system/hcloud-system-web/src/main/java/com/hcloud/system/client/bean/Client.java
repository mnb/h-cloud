package com.hcloud.system.client.bean;

import com.hcloud.common.core.base.BaseBean;
import lombok.Data;

/**
 * @Auther hepangui
 * @Date 2018/12/6
 */
@Data
public class Client extends BaseBean {
    private String clientId;
    private String resourceIds;
    private String clientSecret;
    private String scope;
    private String authorizedGrantTypes;
    private String webServerRedirectUri;
    private String authorities;
    private Integer accessTokenValidity;
    private Integer refreshTokenValidity;
    private String additionalInformation;
    private String autoapprove;
}
