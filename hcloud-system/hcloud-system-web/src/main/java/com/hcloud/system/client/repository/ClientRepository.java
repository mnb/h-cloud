package com.hcloud.system.client.repository;

import com.hcloud.system.client.entity.ClientEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @Auther hepangui
 * @Date 2018/12/06
 */
@Repository
public interface ClientRepository extends BaseRepository<ClientEntity> {
}
