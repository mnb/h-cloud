package com.hcloud.system.role.service.impl;

import com.hcloud.common.crud.service.impl.BaseDataServiceImpl;
import com.hcloud.system.core.bean.role.Role;
import com.hcloud.system.authority.repository.AuthorityRepository;
import com.hcloud.system.role.entity.RoleAuthEntity;
import com.hcloud.system.role.entity.RoleEntity;
import com.hcloud.system.role.repository.RoleAuthRepository;
import com.hcloud.system.role.repository.RoleRepository;
import com.hcloud.system.role.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class RoleServiceImpl extends BaseDataServiceImpl<RoleEntity, RoleRepository, Role> implements RoleService {

    private final RoleAuthRepository roleAuthRepository;

    private final AuthorityRepository authorityRepository;

    @Override
    public List<RoleAuthEntity> findRoleAuthEntityByRoleId(String roleId) {
        return roleAuthRepository.findAllByRoleId(roleId);
    }

    @Override

    public void saveAuth(String roleId, String ids) {
        var allEntityByRoleId = roleAuthRepository.findAllByRoleId(roleId);
        if (allEntityByRoleId != null && allEntityByRoleId.size() > 0) {
            roleAuthRepository.deleteAll(allEntityByRoleId);
        }
        var list = new ArrayList<RoleAuthEntity>();
        Arrays.stream(ids.split(",")).forEach(s -> list.add(RoleAuthEntity.builder().authId(s).roleId(roleId).build()));
        roleAuthRepository.saveAll(list);
    }

    @Override
    public void deleteOne(String id) {
        var allByRoleId = roleAuthRepository.findAllByRoleId(id);
        if (allByRoleId != null && allByRoleId.size() > 0) {
            roleAuthRepository.deleteAll(allByRoleId);
        }
        this.deleteById(id);
    }

    @Override
    public List<String> findAuthorityByRoleIds(List<String> roleIds) {
        var ids = this.roleAuthRepository.findByRoleIdIn(roleIds)
                .stream().map(roleAuthEntity -> roleAuthEntity.getAuthId())
                .collect(Collectors.toList());
        var authoritiesString = this.authorityRepository.findByIdIn(ids)
                .stream().map(authorityEntity ->
                        authorityEntity.getAuthority()).collect(Collectors.toList());
        return authoritiesString;
    }
}
