package com.hcloud.system.authority.controller;

import com.hcloud.auth.api.util.AuthUtil;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.annontion.OperatePostfix;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.controller.BaseTreeController;
import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.system.core.bean.authority.Authority;
import com.hcloud.system.authority.service.AuthorityService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/authority")
@AllArgsConstructor
@AuthPrefix(AuthConstants.SYSTEM_AUTH_PREFIX)
@OperatePostfix("权限")
public class AuthorityController extends BaseTreeController<Authority, Authority> {
    private final AuthorityService authoritiesService;

    @Override
    public BaseDataService getBaseDataService() {
        return authoritiesService;
    }

    /**
     * 本来想直接从认证信息中获取权限信息的，但是为了用户用起来方便，
     * 将token存的比较久，即使关闭浏览器也token也一样有用
     * 这就造成用户token中的权限信息可能是过时的
     * @return
     */
    @GetMapping("getMyAuthorities")
    public HCloudResult<Set<String>> getMyAuthorities(){
        Set<String> authority = AuthUtil.getAuthority();
        return new HCloudResult<>(authority);
    }
}
