package com.hcloud.system.client.service;

import com.hcloud.system.client.bean.Client;
import com.hcloud.system.client.entity.ClientEntity;
import com.hcloud.common.crud.service.BaseDataService;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface ClientService
        extends BaseDataService<ClientEntity, Client> {

    void modifyPass(String id, String oldPsw, String newPsw);

    void resetPass(String userId);
}
