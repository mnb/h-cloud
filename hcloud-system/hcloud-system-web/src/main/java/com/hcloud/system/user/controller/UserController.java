package com.hcloud.system.user.controller;

import com.hcloud.audit.core.annontion.OperateLog;
import com.hcloud.audit.core.util.OperateType;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.annontion.OperatePostfix;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.auth.api.util.AuthUtil;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.exception.ServiceException;
import com.hcloud.common.crud.annon.HCloudPreAuthorize;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.social.service.QQInfoService;
import com.hcloud.common.social.service.WeixinInfoService;
import com.hcloud.system.core.bean.user.User;
import com.hcloud.system.core.bean.user.UserQuery;
import com.hcloud.system.user.entity.UserEntity;
import com.hcloud.system.user.service.UserService;
import lombok.AllArgsConstructor;
import com.hcloud.common.crud.service.BaseDataService;
import org.springframework.data.domain.Example;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
@AuthPrefix(AuthConstants.SYSTEM_USER_PREFIX)
@OperatePostfix("用户")
public class UserController extends BaseDataController<User, UserQuery> {

    private final UserService userService;

    private WeixinInfoService weixinInfoService;
    private QQInfoService qqInfoService;

    @Override
    public BaseDataService getBaseDataService() {
        return userService;
    }

    @GetMapping("/getByAccount/{account}")
    public HCloudResult getUserByAccount(@PathVariable String account) {
        return new HCloudResult(userService.findByAccount(account));
    }

    @GetMapping("/getByMobile/{para}")
    public HCloudResult getByMobile(@PathVariable String para) {
        return new HCloudResult(userService.findByMobile(para));
    }

    @GetMapping("/getByWeixin/{para}")
    public HCloudResult getByWeixin(@PathVariable String para) {
        return new HCloudResult(userService.findByWeixin(para));
    }

    @GetMapping("/getByQq/{para}")
    public HCloudResult getByQq(@PathVariable String para) {
        return new HCloudResult(userService.findByQq(para));
    }


    @PostMapping("/modifyPass")
    @OperateLog(title = "修改密码",type = OperateType.UPDATE)
    public HCloudResult modifyPass(String oldPsw,String newPsw){
        User user = AuthUtil.getUser();
        this.userService.modifyPass(user,oldPsw,newPsw);
        return new HCloudResult();
    }

    @PostMapping("/modifyInfo")
    @OperateLog(title = "修改个人信息",type = OperateType.UPDATE)
    public HCloudResult modifyInfo(User user){
        this.userService.update(user);
        return new HCloudResult();
    }

    @PostMapping("/bind/weixin")
    @OperateLog(title = "绑定微信",type = OperateType.UPDATE)
    public HCloudResult binWeixin(String code){
        String openId =null;
        try{
             openId = this.weixinInfoService.getOpenId(code);
        }catch (Exception e){
            return new HCloudResult("绑定微信失败");
        }
        var user = AuthUtil.getUser();
        var userEntity = this.userService.get(user.getId());
        if(userEntity !=null){
            userEntity.setWechat(openId);
            this.userService.update(userEntity);
        }
        return new HCloudResult();
    }

    @PostMapping("/bind/qq")
    @OperateLog(title = "绑定qq",type = OperateType.UPDATE)
    public HCloudResult bindQq(String code){
        String openId =null;
        try{
            openId = this.qqInfoService.getOpenId(code);
        }catch (Exception e){
            return new HCloudResult("绑定qq失败");
        }
        var user = AuthUtil.getUser();
        var userEntity = this.userService.get(user.getId());
        if(userEntity !=null){
            userEntity.setQq(openId);
            this.userService.update(userEntity);
        }
        return new HCloudResult();
    }


    @PostMapping("/resetPass")
    @OperateLog(title = "重置密码",type = OperateType.UPDATE)
    @HCloudPreAuthorize(AuthConstants.EDIT)
    public HCloudResult resetPass(String userId){
        this.userService.resetPass(userId);
        return new HCloudResult();
    }
}
