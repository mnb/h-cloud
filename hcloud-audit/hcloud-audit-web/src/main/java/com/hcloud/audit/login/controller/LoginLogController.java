package com.hcloud.audit.login.controller;

import com.hcloud.audit.core.bean.LoginLog;
import com.hcloud.audit.core.bean.LoginLogQuery;
import com.hcloud.audit.login.service.LoginLogService;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/login")
@AllArgsConstructor
@AuthPrefix(AuthConstants.AUDIT_LOGIN_PREFIX)
public class LoginLogController extends BaseDataController<LoginLog, LoginLogQuery> {
    private final LoginLogService loginLogService;


    @Override
    public BaseDataService getBaseDataService() {
        return loginLogService;
    }

    /**
     * 重写方法，放开权限
     * @param bean
     * @return
     */
    @PostMapping(value="/save")
    public HCloudResult save(@RequestBody LoginLog bean) {
        return super.saveOne(bean);
    }
}
