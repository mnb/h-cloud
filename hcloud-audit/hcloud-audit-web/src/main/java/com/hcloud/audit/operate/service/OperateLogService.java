package com.hcloud.audit.operate.service;

import com.hcloud.audit.core.bean.OperateLog;
import com.hcloud.audit.operate.entity.OperateLogEntity;
import com.hcloud.common.crud.service.BaseDataService;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface OperateLogService
        extends BaseDataService<OperateLogEntity, OperateLog> {

}
