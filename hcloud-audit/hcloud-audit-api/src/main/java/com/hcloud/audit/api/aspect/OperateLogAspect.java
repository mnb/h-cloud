package com.hcloud.audit.api.aspect;

import com.alibaba.fastjson.JSONObject;
import com.hcloud.audit.api.event.OperateLogEvent;
import com.hcloud.audit.core.bean.OperateLog;
import com.hcloud.audit.core.util.IPUtils;
import com.hcloud.audit.core.util.OperateType;
import com.hcloud.auth.api.util.AuthUtil;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.annontion.OperatePostfix;
import com.hcloud.common.core.util.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Aspect
@Slf4j
@Component
@Order(30)
public class OperateLogAspect {

    @Around("@annotation(operateLog)")
    public Object around(ProceedingJoinPoint point, com.hcloud.audit.core.annontion.BaseOperateLog operateLog) throws Throwable {
        var target = point.getTarget();
        var annotation = target.getClass().getAnnotation(OperatePostfix.class);
        if (annotation == null) {
            return point.proceed();
        } else {
            var type = operateLog.type();
            var title = getTitle(type,annotation.value());
            var beginTime = System.currentTimeMillis();
            var result = point.proceed();
            var time = System.currentTimeMillis() - beginTime;
            var args = point.getArgs();
            saveLog(title, type,args, time);
            return result;
        }


    }


    @Around("@annotation(operateLog)")
    public Object around(ProceedingJoinPoint point, com.hcloud.audit.core.annontion.OperateLog operateLog) throws Throwable {
        var title = operateLog.title();
        var type = operateLog.type();
        var beginTime = System.currentTimeMillis();
        var result = point.proceed();
        var time = System.currentTimeMillis() - beginTime;
        var args = point.getArgs();
        saveLog(title,type,args, time);
        return result;
    }

    private String getTitle(String type, String value) {
        switch (type){
            case OperateType.ADD : return "新增"+value;
            case OperateType.UPDATE : return "修改"+value;
            case OperateType.QUERY : return "查询"+value;
            case OperateType.DEL : return "删除"+value;
        }
        return "";
    }

    private void saveLog(String title, String type, Object [] args, long time) {
        try {
            var params = ArrayUtils.toString(args);
            var request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            var requestURI = request.getRequestURI();

            var operateLogBean = OperateLog.builder().params(params)
                    .type(type)
                    .uri(requestURI)
                    .ip(IPUtils.getIpAddr(request))
                    .operate(title)
                    .username(AuthUtil.getUser().getName())
                    .time(time).build();
            SpringUtil.publishEvent(new OperateLogEvent(operateLogBean));
        } catch (Exception e) {
            log.error("保存操作日志失败", e);
        }

    }
}
