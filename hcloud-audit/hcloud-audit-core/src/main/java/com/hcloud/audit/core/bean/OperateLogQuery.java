package com.hcloud.audit.core.bean;

import lombok.Data;

import java.util.Date;

/**
 * @Auther hepangui
 * @Date 2018/11/29
 */
@Data
public class OperateLogQuery extends OperateLog {
    private String usernameLike;
    private Date createTimeLt;
    private Date createTimeGt;
}
