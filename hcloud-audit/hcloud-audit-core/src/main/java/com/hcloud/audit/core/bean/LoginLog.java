package com.hcloud.audit.core.bean;

import com.hcloud.common.core.base.BaseBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginLog extends BaseBean  {
    private String account;
    private String username;
    private String osName;
    private String device;
    private String browser;
    private String ipAddr;
    private Integer success;
    private String msg;
}
