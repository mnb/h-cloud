package com.hcloud.audit.core.util;

/**
 * @Auther hepangui
 * @Date 2018/11/29
 */
public interface OperateType {
    String ADD = "add";
    String UPDATE = "update";
    String DEL = "del";
    String QUERY ="query";
}
