package com.hcloud.audit.core.bean;

import lombok.Data;

import java.util.Date;

/**
 * @Auther hepangui
 * @Date 2018/11/29
 */
@Data
public class LoginLogQuery extends LoginLog {
    private String usernameLike;
    private Date createTimeLt;
    private Date createTimeGt;
}
